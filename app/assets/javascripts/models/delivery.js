// for more details see: http://emberjs.com/guides/models/defining-models/

Dispo.Delivery = DS.Model.extend({
  deliveryDate: DS.attr(),
  active: DS.attr(),

  bruttoAmount: DS.attr(),
  nettoAmount: DS.attr(),

  pallets: DS.hasMany('pallet', {async: true}),
  availablePallets: DS.hasMany('availablePallet', {async: true})
});
