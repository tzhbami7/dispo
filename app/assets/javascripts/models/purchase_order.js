// for more details see: http://emberjs.com/guides/models/defining-models/

Dispo.PurchaseOrder = DS.Model.extend({
  baanId: DS.attr('string'),
  tradingPartner: "Sanitas Troesch AG, Simonstrasse 5, CH-9016 St. Gallen",
  
  orderItems: DS.hasMany('orderItem', {async: true}),
  pallets: DS.hasMany('pallet', {async: true})
});