// for more details see: http://emberjs.com/guides/models/defining-models/

Dispo.LineItem = DS.Model.extend({
  availableQuantity: function() {
    return this.get('orderItem.availableQuantity');
  }.property('orderItem.availableQuantity'),
  increaseDeactivated: function() {
    if (this.get('availableQuantity') != 0) {
      return false;
    } else {
      return true;
    }
  }.property('availableQuantity'),
  decreaseDeactivated: function() {
    if (this.get('quantity') != 1) {
      return false;
    } else {
      return true;
    }
  }.property('quantity'),

  quantity: DS.attr(),
  bruttoWeight: DS.attr(),
  bruttoAmount: DS.attr(),
  nettoWeight: DS.attr(),
  nettoAmount: DS.attr(),
  createdAt: DS.attr(),
  updatedAt: DS.attr(),

  orderItem: DS.belongsTo('orderItem', {async: true}),
  pallet: DS.belongsTo('pallet', {async: true})
});