// for more details see: http://emberjs.com/guides/models/defining-models/

Dispo.Importer = DS.Model.extend({
  baanFile: DS.attr(),
  contentType: DS.attr(),
  filename: DS.attr(),
  baanFileUrl: DS.attr()
});
