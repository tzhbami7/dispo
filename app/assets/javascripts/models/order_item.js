// for more details see: http://emberjs.com/guides/models/defining-models/

Dispo.OrderItem = DS.Model.extend({
  selectedQuantity: 0,
  increaseDeactivated: function() {
    if (this.get('availableQuantity') != 0) {
      return false;
    } else {
      return true;
    }
  }.property('availableQuantity'),
  decreaseDeactivated: function() {
    if (this.get('selectedQuantity') != 0) {
      return false;
    } else {
      return true;
    }
  }.property('selectedQuantity'),

  baanId: DS.attr(),
  quantity: DS.attr(),
  availableQuantity: DS.attr(),

  purchaseOrder: DS.belongsTo('purchaseOrder', {async: true}),
  article: DS.belongsTo('article', {async: true})
});