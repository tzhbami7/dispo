// for more details see: http://emberjs.com/guides/models/defining-models/

Dispo.Article = DS.Model.extend({
  name: DS.attr(),
  articleNumber: DS.attr(),
  baanId: DS.attr(),

  orderItem: DS.belongsTo('orderItem', {async: true})
});