// for more details see: http://emberjs.com/guides/models/defining-models/

Dispo.Pallet = DS.Model.extend({
  isSelected: false,
  panelClass: function() {
    if (this.get('isSelected')) {
      return "panel panel-success";
    } else {
      return "panel panel-default";
    };
  }.property('isSelected'),

  bruttoWeight: DS.attr(),
  bruttoAmount: DS.attr(),
  nettoWeight: DS.attr(),
  nettoAmount: DS.attr(),
  createdAt: DS.attr(),
  updatedAt: DS.attr(),

  purchaseOrder: DS.belongsTo('purchaseOrder', {async: true}),
  delivery: DS.belongsTo('delivery', {async: true}),
  lineItems: DS.hasMany('lineItem', {async: true})
});

Dispo.AvailablePallet = DS.Model.extend({
  isSelected: false,
  panelClass: function() {
    if (this.get('isSelected')) {
      return "panel panel-success";
    } else {
      return "panel panel-default";
    };
  }.property('isSelected'),

  bruttoWeight: DS.attr(),
  bruttoAmount: DS.attr(),
  nettoWeight: DS.attr(),
  nettoAmount: DS.attr(),
  createdAt: DS.attr(),
  updatedAt: DS.attr(),

  purchaseOrder: DS.belongsTo('purchaseOrder', {async: true}),
  delivery: DS.belongsTo('delivery', {async: true}),
  lineItems: DS.hasMany('lineItem', {async: true})
});
