Dispo.DeliveryRoute = Ember.Route.extend({
  model: function(params) {
    return this.store.findById('delivery', params.delivery_id);
  }
});

Dispo.DeliveryIndexRoute = Ember.Route.extend({
  model: function() {
    return this.modelFor('delivery');
  }
});