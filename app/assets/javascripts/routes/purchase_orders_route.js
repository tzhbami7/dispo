Dispo.PurchaseOrdersRoute = Ember.Route.extend({
});

Dispo.PurchaseOrdersIndexRoute = Ember.Route.extend({
  setupController: function(controller, model) {
    // Set the IndexController's `title`
    controller.set('title', "VK-Aufträäge");
    controller.set('model', model);
  },
  model: function() {
    return this.store.find('purchaseOrder');
  }
});