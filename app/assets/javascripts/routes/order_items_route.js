Dispo.OrderItemsRoute = Ember.Route.extend({
});

Dispo.OrderItemsIndexRoute = Ember.Route.extend({
  setupController: function(controller, model) {
    // Set the IndexController's `title`
    controller.set('title', "VK-Positionen");
    controller.set('model', model);
  },
  model: function() {
    return this.store.find('orderItem');
  }
});