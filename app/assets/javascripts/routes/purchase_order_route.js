Dispo.PurchaseOrderRoute = Ember.Route.extend({
  model: function(params) {
    return this.store.findById('purchaseOrder', params.purchase_order_id);
  }
});

Dispo.PurchaseOrderIndexRoute = Ember.Route.extend({
  model: function() {
    return this.modelFor('purchaseOrder');
  }
});