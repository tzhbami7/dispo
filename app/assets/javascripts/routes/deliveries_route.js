Dispo.DeliveriesRoute = Ember.Route.extend({
});

Dispo.DeliveriesIndexRoute = Ember.Route.extend({
  setupController: function(controller, model) {
    controller.set('model', model);
  },
  model: function() {
    return this.store.find('delivery');
  }
});

Dispo.DeliveriesNewRoute = Ember.Route.extend({
  setupController: function(controller, model) {
    controller.set('model', model);
  },
  model: function() {
    return this.store.createRecord('delivery');
  }
});