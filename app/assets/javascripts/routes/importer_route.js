Dispo.ImporterRoute = Ember.Route.extend({
});

Dispo.ImporterIndexRoute = Ember.Route.extend({
  setupController: function(controller, model) {
    controller.set('model', model);
  },
  model: function() {
    return this.store.find('importer');
  }
});

Dispo.ImporterNewRoute = Ember.Route.extend({
  setupController: function(controller, model) {
    controller.set('model', model);
  },
  model: function() {
    return this.store.createRecord('importer');
  }
});
