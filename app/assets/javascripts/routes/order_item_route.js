Dispo.OrderItemRoute = Ember.Route.extend({
  setupController: function(controller, model) {
    // Set the IndexController's `title`
    //
    controller.set('model', model);
    //controller.set('content', model)
  },
  model: function(params) {
    return this.store.findById('orderItem', params.order_item_id);
  }
});