//= require jquery
//= require jquery_ujs
//= require twitter/bootstrap
//= require handlebars
//= require ember
//= require ember-data
//= require_self
//= require dispo

// for more details see: http://emberjs.com/guides/application/
Dispo = Ember.Application.create({
  LOG_TRANSITIONS: true,
  LOG_TRANSITIONS_INTERNAL: true,
  LOG_VIEW_LOOKUPS: true,
  LOG_ACTIVE_GENERATION: true
});

Dispo.ApplicationAdapter = DS.ActiveModelAdapter.extend()
Dispo.ApplicationSerializer = DS.ActiveModelSerializer.extend();
