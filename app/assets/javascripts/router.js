// For more information see: http://emberjs.com/guides/routing/

Dispo.Router.map(function() {
  this.resource('purchase_order', { path: '/purchase_orders/:purchase_order_id' }, function() {
    this.route('edit');
  });

  this.resource('purchase_orders', function() {
    this.route('new');
  });

  this.resource('order_items', function() {
    this.route('new');
  });

  this.resource('order_item', { path: '/order_items/:order_item_id' }, function() {
    this.route('edit');
  });

  this.resource('delivery', { path: '/deliveries/:delivery_id' }, function() {
    this.route('edit');
  });

  this.resource('deliveries', function() {
    this.route('new');
  });

  this.resource('importer', function() {
      this.route('new')
  });
});
