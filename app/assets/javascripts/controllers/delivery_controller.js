Dispo.DeliveryController = Ember.ObjectController.extend({
});

Dispo.DeliveryIndexController = Ember.ObjectController.extend({
  palletAssignmentDisabled: true,
  selectedPallet: null,

  actions: {
    assignPallet: function(pallet) {
      var self = this;
      pallet.set('delivery', this.model);

      pallet.save().then(function() {
        self.model.reload();
      });
    },
    removePallet: function(pallet) {
      var self = this;
      pallet.set('delivery', undefined);

      pallet.save().then(function() {
        self.model.reload();
      });
    },

    bla: function() {

    }
  }
});
