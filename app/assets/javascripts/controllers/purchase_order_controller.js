Dispo.PurchaseOrderController = Ember.ObjectController.extend({
});

Dispo.PurchaseOrderIndexController = Ember.ObjectController.extend({
  //pallets: Dispo.Pallet.store.findQuery('pallet', {ids: 1}),
  lineItemAssignmentDisabled: true,
  selectedPallet: null,

  actions: {
    selectPallet: function(pallet) {
      if (this.get('selectedPallet') != null) {
        this.get('selectedPallet').set('isSelected', false);
      };
      this.set('selectedPallet', pallet);
      pallet.set('isSelected', true);
      this.set('lineItemAssignmentDisabled', false);
    },
    resetMountedQuantity: function(orderItem) {
      var newSelectedQuantity = 0;
      var newAvailableQuantity = orderItem.get('selectedQuantity') + orderItem.get('availableQuantity');
      orderItem.set('selectedQuantity', newSelectedQuantity);
      orderItem.set('availableQuantity', newAvailableQuantity);
      // orderItem.set('notAvailable', false);
    },

    increaseMountedQuantityToMax: function(orderItem) {
      var newSelectedQuantity = orderItem.get('selectedQuantity') + orderItem.get('availableQuantity');
      var newAvailableQuantity = 0;
      orderItem.set('selectedQuantity', newSelectedQuantity);
      orderItem.set('availableQuantity', newAvailableQuantity);
      //orderItem.set('notAvailable', true);
    },
    increaseAssignedQuantityToMax: function(lineItem) {
      var newQuantity = lineItem.get('quantity') + lineItem.get('availableQuantity');

      lineItem.set('quantity', newQuantity);
      lineItem.save().then(function() {

      });
    },

    increaseMountedQuantity: function(orderItem) {
      var newSelectedQuantity = orderItem.get('selectedQuantity') + 1;
      var newAvailableQuantity = orderItem.get('availableQuantity') - 1;
      orderItem.set('selectedQuantity', newSelectedQuantity);
      orderItem.set('availableQuantity', newAvailableQuantity);
      //if (newAvailableQuantity == 0) {orderItem.set('notAvailable', true)};
    },
    increaseAssignedQuantity: function(lineItem) {
      var newQuantity = lineItem.get('quantity') + 1;
      var orderItem = lineItem.get('orderItem');

      lineItem.set('quantity', newQuantity);

      lineItem.save().then(function() {
        console.log(orderItem);
      });
    },

    decreaseMountedQuantity: function(orderItem) {
      var newSelectedQuantity = orderItem.get('selectedQuantity') - 1;
      var newAvailableQuantity = orderItem.get('availableQuantity') + 1;
      orderItem.set('selectedQuantity', newSelectedQuantity);
      orderItem.set('availableQuantity', newAvailableQuantity);
    },
    decreaseAssignedQuantity: function(lineItem) {
      var newQuantity = lineItem.get('quantity') - 1;
      var orderItem = lineItem.get('orderItem');

      lineItem.set('quantity', newQuantity);

      lineItem.save().then(function() {
        console.log(orderItem);
      });
    },

    assignLineItems: function(orderItem) {
      var pallet = this.get('selectedPallet');
      var lineItem = this.store.createRecord('lineItem', {
        orderItem: orderItem,
        pallet: pallet,
        quantity: orderItem.get('selectedQuantity')
      });

      lineItem.save().then(function() {
        orderItem.set('selectedQuantity', 0);

        pallet.reload();
        orderItem.reload();
        //lineItem.set('pallet', pallet);
        //lineItem.set('orderItem', orderItem);
      });
    },
    newPallet: function(purchaseOrder) {
      var pallet = this.store.createRecord('pallet', {
        purchaseOrder: purchaseOrder
      });

      pallet.save().then(function() {
      purchaseOrder.reload();
      });
    },

    removePallet: function(pallet) {
      pallet.deleteRecord();

      pallet.save().then(function() {
        console.log('pallet destroyed...');
      });
    },
    removeLineItem: function(lineItem) {
      lineItem.deleteRecord();

      lineItem.save().then(function() {
        console.log('lineItem destroyed...');
      });
    }
  }
});