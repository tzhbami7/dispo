Dispo.DeliveriesController = Ember.ArrayController.extend({
  
});

Dispo.DeliveriesIndexController = Ember.ArrayController.extend({
  
});

Dispo.DeliveriesNewController = Ember.ObjectController.extend({
  //pallets: Dispo.Pallet.store.findQuery('pallet', {ids: 1}),
  lineItemAssignmentDisabled: true,
  selectedPallet: null,

  actions: {
    submitDelivery: function() {
      var self = this;
      this.get('model').save().then(function() {
        self.transitionToRoute('deliveries.index');
      });
    }
  }
});
