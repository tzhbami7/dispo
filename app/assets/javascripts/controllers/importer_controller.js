Dispo.ImporterController = Ember.ArrayController.extend({

});

Dispo.ImporterIndexController = Ember.ArrayController.extend({

});

Dispo.ImporterNewController = Ember.ObjectController.extend({
  actions: {
    testFoo: function(data) {
      var self = this;

      var reader = new FileReader();
      var file = reader.readAsBinaryString(data[0]);

      self.set('contentType', data[0].type);
      self.set('filename', data[0].name);

      reader.onload = function(e) {
        self.set('baanFile', btoa(reader.result));
      }
    },
    submitAction: function() {
      var self = this;
      
      this.get('model').save().then(function() {
        self.transitionToRoute('importer.index');
      });
    }
  }
});
