Dispo.UploadField = Ember.TextField.extend({
    type: "file",
    multiple: false,
    change: function(e) {
      var input = e.target;

      if (!Ember.isEmpty(input.files)) {
        this.sendAction("foo", input.files);
      }
    }
});
