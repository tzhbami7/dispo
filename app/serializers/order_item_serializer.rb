class OrderItemSerializer < ActiveModel::Serializer
  embed :ids, include: false
  attributes :id, :baan_id, :purchase_order_id, :article_id, :quantity, :available_quantity

  has_one :purchase_order
  has_one :article

end