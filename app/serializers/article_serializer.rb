class ArticleSerializer < ActiveModel::Serializer
  embed :ids, include: false
  attributes :id, :baan_id, :name, :article_number

end