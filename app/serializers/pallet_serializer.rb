class PalletSerializer < ActiveModel::Serializer
  embed :ids, include: false
  attributes :id, :brutto_amount, :brutto_weight, :discount_rate, :netto_amount, :netto_weight, :purchase_order_id, :delivery_id#, :purchase_order

  # has_one :purchase_order
  has_many :line_items
  has_one :delivery
end
