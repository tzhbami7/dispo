class DeliverySerializer < ActiveModel::Serializer
  embed :ids, include: false
  attributes :id, :active, :delivery_date, :brutto_amount, :netto_amount

  has_many :pallets, :include => true
  has_many :available_pallets, :serializer => PalletSerializer, :include => true

  def netto_amount
      return object.pallets.sum(:netto_amount)
  end

  def brutto_amount
      return object.pallets.sum(:brutto_amount)
  end

  def available_pallets
    return Pallet.where(:delivery_id => nil).where("pallets.purchase_order_id IS NOT NULL")
  end

end
