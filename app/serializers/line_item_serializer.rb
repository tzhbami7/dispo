class LineItemSerializer < ActiveModel::Serializer
  embed :ids, include: false
  attributes :id, :brutto_amount, :brutto_weight, :netto_amount, :netto_weight, :order_item_id, :variant_id, :pallet_id, :quantity

  has_one :order_item, include: true, :root => :order_item

end