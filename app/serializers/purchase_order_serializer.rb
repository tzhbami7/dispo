class PurchaseOrderSerializer < ActiveModel::Serializer
  embed :ids, include: false
  attributes :id, :baan_id

  has_many :order_items
  has_many :pallets
end
