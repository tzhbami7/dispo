class ImporterSerializer < ActiveModel::Serializer
  embed :ids, include: false
  attributes :id, :content_type, :filename, :baan_file_url, :created_at, :updated_at

  def filename
    return object.baan_file.file.filename
  end

  def content_type
    return object.baan_file.file.content_type
  end

  def baan_file_url
    return object.baan_file_url
  end

end
