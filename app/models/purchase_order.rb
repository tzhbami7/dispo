class PurchaseOrder < ActiveRecord::Base
  attr_accessible :baan_id

  belongs_to :delivery_address
  belongs_to :invoice_address
  belongs_to :payer_address

  has_many :order_items
  has_many :pallets

end
