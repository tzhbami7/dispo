class Article < ActiveRecord::Base
  attr_accessible :article_number, :name
  has_many :order_items
  
  before_create :generate_baan_id

  private
    def generate_baan_id
      self.baan_id = Digest::MD5.hexdigest(self.article_number.to_s) unless !self.article_number.present?
    end
end
