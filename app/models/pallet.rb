class Pallet < ActiveRecord::Base
  attr_accessible :brutto_amount, :brutto_weight, :discount_rate, :netto_amount, :netto_weight, :purchase_order_id, :shipping_id, :delivery_id

  belongs_to :purchase_order
  belongs_to :delivery

  has_many :line_items, :dependent => :destroy

  accepts_nested_attributes_for :purchase_order

end
