class Variant < ActiveRecord::Base
  attr_accessible :brutto_amount, :brutto_weight, :netto_amount, :netto_weight, :order_item_id

  belongs_to :order_item
  belongs_to :line_item

end
