class LineItem < ActiveRecord::Base
  attr_accessible :brutto_amount, :brutto_weight, :netto_amount, :netto_weight, :order_item_id, :variant_id, :pallet_id, :quantity

  belongs_to :order_item
  belongs_to :variant
  belongs_to :pallet

  after_save :update_order_item
  after_destroy :update_order_item

  def update_order_item
    self.order_item.save
  end

end
