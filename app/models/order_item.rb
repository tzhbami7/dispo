class OrderItem < ActiveRecord::Base
  attr_accessible :article_id, :baan_id, :purchase_order_id, :quantity, :stocked, :manufactured, :depot, :netto_amount, :brutto_amount, :weight, :picked_up

  belongs_to :purchase_order
  belongs_to :article
  belongs_to :commodity

  belongs_to :delivery_address
  belongs_to :invoice_address
  belongs_to :payer_address

  belongs_to :delivery_group
  belongs_to :delivery_location

  has_many :line_items
  has_many :variants

  before_save :update_available_quantity

  def update_available_quantity
    self.available_quantity = self.quantity - self.line_items.sum(:quantity)
    #self.save
  end
end
