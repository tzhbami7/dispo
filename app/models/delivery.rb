class Delivery < ActiveRecord::Base
  attr_accessible :active, :delivery_date

  has_many :pallets
end
