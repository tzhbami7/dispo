class Importer < ActiveRecord::Base
    attr_accessible :baan_file
    mount_uploader :baan_file, BaanFileUploader
end
