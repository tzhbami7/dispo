class InvoiceAddress < ActiveRecord::Base
  attr_accessible :eori, :country, :baan_id, :company_name, :street, :postal_code, :city
  belongs_to :customer
end
