class DeliveriesController < ApplicationController

  def show
    delivery = Delivery.where(:id => params[:id]).first
    if delivery
      render :json => delivery, :serializer => DeliverySerializer, :root => :delivery
    else
      head 404 
    end
  end

  def index
    deliveries = Delivery.order("id DESC")
    render :json => deliveries, :each_serializer => DeliverySerializer, :root => :deliveries
  end

  def create
    delivery = Delivery.new
    if delivery.save
      render :json => delivery, :serializer => DeliverySerializer, :root => :delivery
    else
      head :conflict
    end
  end
end
