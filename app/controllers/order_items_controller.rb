class OrderItemsController < ApplicationController
  def index
    order_items = OrderItem.includes(:purchase_order, :article).where(:id => params[:ids])
    render :json => order_items, :each_serializer => OrderItemSerializer, :root => :order_items
  end

  def show
    order_item = OrderItem.includes(:purchase_order, :article).where(:id => params[:id]).first
    render :json => order_item, :serializer => OrderItemSerializer, :root => :order_item
  end

end