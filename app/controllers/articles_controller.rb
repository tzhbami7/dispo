class ArticlesController < ApplicationController

  def index
    articles = Article.order("id DESC")
    render :json => articles, :each_serializer => ArticleSerializer, :root => :articles
  end

  def show
    article = Article.where(:id => params[:id]).first
    render :json => article, :serializer => ArticleSerializer, :root => :article
  end

end
