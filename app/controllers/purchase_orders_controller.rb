class PurchaseOrdersController < ApplicationController

  def index
    purchase_orders = PurchaseOrder.includes(:order_items)
    render :json => purchase_orders, :each_serializer => PurchaseOrderSerializer, :root => :purchase_orders
  end

  def show
    purchase_order = PurchaseOrder.includes(:pallets => [:line_items => :order_item], :order_items => [:article]).where(:id => params[:id]).first
    render :json => purchase_order, :serializer => PurchaseOrderSerializer, :root => :purchase_order
  end

end
