class ImportersController < ApplicationController
  before_filter :decode_upload, :only => :create

  def index
    importers = Importer.order("id DESC")
    render :json => importers, :each_serializer => ImporterSerializer, :root => :importers
  end

  def show
    head 404
  end

  def create
    importer = Importer.new
    importer.baan_file = params[:importer][:baan_file]

    if importer.save
      render :json => importer, :serializer => ImporterSerializer, :root => :importer
    else
      head :conflict
    end
  end

  private

  def decode_upload
    filename = params[:importer][:filename]

    tempfile = Tempfile.new("#{filename}")
    tempfile.binmode
    tempfile.write(Base64.decode64(params[:importer][:baan_file]))

    uploaded_file = ActionDispatch::Http::UploadedFile.new(:tempfile => tempfile, :filename => filename)

    params[:importer][:baan_file] = uploaded_file
  end
end
