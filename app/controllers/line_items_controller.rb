class LineItemsController < ApplicationController
  def show
    line_item = LineItem.where(:id => params[:id]).first
    if line_item
      render :json => line_item, :serializer => LineItemSerializer, :root => :line_item
    else
      head 404
    end
  end

  def index
    line_items = LineItem.where(:id => params[:ids])
    if !line_items.empty?
      render :json => line_items, :each_serializer => LineItemSerializer, :root => :line_items
    else
      head :conflict
    end
  end

  def update
    line_item = LineItem.where(:id => params[:id]).first

    if line_item
      line_item.quantity = params[:line_item][:quantity]
      if line_item.save
        render :json => line_item, :serializer => LineItemSerializer, :root => :line_item
      else
        head :conflict
      end
    else
      head 404
    end
  end

  def create
    pallet = Pallet.where(:id => params[:line_item][:pallet_id]).first
    order_item = OrderItem.where(:id => params[:line_item][:order_item_id]).first
    line_item = pallet.line_items.where(:order_item_id => params[:line_item][:order_item_id]).first

    if pallet && order_item
      if line_item
        line_item.quantity += params[:line_item][:quantity]
      else
        line_item ||= pallet.line_items.build(params[:line_item])
      end

      if line_item.save
        render :json => line_item, :serializer => LineItemSerializer, :root => :line_item
      else
        head :conflict
      end
    else
      head 404    
    end
  end

  def destroy
    line_item = LineItem.where(params[:id]).first

    if line_item.destroy
      render :json => line_item, :serializer => LineItemSerializer, :root => :line_item
      # head :ok
    else
      head :bad_request
    end
  end

end
