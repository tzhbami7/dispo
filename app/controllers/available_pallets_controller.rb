class AvailablePalletsController < ApplicationController

  def index

  end

  def show
    pallet = Pallet.includes(:line_items).where(:id => params[:id]).first

    render :json => pallet, :serializer => PalletSerializer, :root => :available_pallet
  end

  def update
    pallet = Pallet.includes(:line_items).where(:id => params[:id]).first
    if pallet.update_attributes(params[:available_pallet])
      render :json => pallet, :serializer => PalletSerializer, :root => :available_pallet
    else
      head :bad_request
    end
  end

end
