class DeliveryGroupWorker
  include Sidekiq::Worker
  sidekiq_options :retry => true, :backtrace => true

  def perform(attributes, relations)
    delivery_group = DeliveryGroup.where(:name => attributes["name"]).first
    delivery_group ||= DeliveryGroup.new(attributes)

    delivery_group.save if delivery_group.valid?
  end

  def self.import(csv_row, async=true)
    mapper = Mapper::DeliveryGroup.new(csv_row)
    self.perform_async(mapper.attributes, mapper.relations)
  end

end
