class DeliveryLocationWorker
  include Sidekiq::Worker
  sidekiq_options :retry => true, :backtrace => true

  def perform(attributes, relations)
    delivery_location = DeliveryLocation.where(:name => attributes["name"]).first
    delivery_location ||= DeliveryLocation.new(attributes)

    delivery_location.save if delivery_location.valid?
  end

  def self.import(csv_row, async=true)
    mapper = Mapper::DeliveryLocation.new(csv_row)
    self.perform_async(mapper.attributes, mapper.relations)
  end

end
