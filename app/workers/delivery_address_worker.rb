class DeliveryAddressWorker
  include Sidekiq::Worker
  sidekiq_options :retry => true, :backtrace => true

  def perform(attributes, relations)
    address = DeliveryAddress.where(:baan_id => attributes["baan_id"]).first
    address ||= DeliveryAddress.new

    address.attributes = attributes
    address.save

    if relations
      RelationWorker.delivery_address(address.id).set(relations["customer"]) if address.customer_id.nil?
    end
  end

  def self.import(csv_row, async=true)
    mapper = Mapper::DeliveryAddress.new(csv_row)
    self.perform_async(mapper.attributes, mapper.relations)
  end

end
