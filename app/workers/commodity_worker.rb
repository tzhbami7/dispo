class CommodityWorker
  include Sidekiq::Worker
  sidekiq_options :retry => true, :backtrace => true

  def perform(attributes, relations)
    commodity = Commodity.where(:code => attributes["code"]).first
    commodity ||= Commodity.new(attributes)

    commodity.save if commodity.valid?
  end

  def self.import(csv_row, async=true)
    mapper = Mapper::Commodity.new(csv_row)
    self.perform_async(mapper.attributes, mapper.relations)
  end

end
