class RowDataWorker
  include Sidekiq::Worker
  sidekiq_options :retry => true, :backtrace => true

  def perform(csv_row)
    DeliveryGroupWorker.import(csv_row, true)
    DeliveryLocationWorker.import(csv_row, true)
    
    CustomerWorker.import(csv_row, true)
    CommodityWorker.import(csv_row, true)

    InvoiceAddressWorker.import(csv_row, true)
    PayerAddressWorker.import(csv_row, true)
    DeliveryAddressWorker.import(csv_row, true)

    PurchaseOrderWorker.import(csv_row, true)
    OrderItemWorker.import(csv_row, true)
  end

  def self.import_row(csv_row)
    self.perform_async(csv_row)
  end
end
