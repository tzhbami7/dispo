require "csv"

class CSVWorker
  include Sidekiq::Worker
  sidekiq_options :retry => true, :backtrace => true

  def perform(data)
    csv_file = ::CSV.open(data, "rb:iso-8859-1:UTF-8", self.class.csv_options)

    csv_file.each do |csv_row|
      RowDataWorker.import_row(csv_row)
    end
  end

  def self.load_csv(csv_path)
    self.perform_async(csv_path)
  end

  private

  def self.csv_options
    return {:col_sep => ";", :headers => :first_row}
  end

end
