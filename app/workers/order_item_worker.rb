class OrderItemWorker
  include Sidekiq::Worker
  sidekiq_options :retry => true, :backtrace => true

  def perform(attributes, relations)
    order_item = OrderItem.where(:baan_id => attributes["baan_id"]).first
    order_item ||= OrderItem.new(attributes)

    order_item.save

    if relations
      RelationWorker.order_item(order_item.id).set(relations["delivery_group"])
      RelationWorker.order_item(order_item.id).set(relations["delivery_location"])

      RelationWorker.order_item(order_item.id).set(relations["payer_address"])
      RelationWorker.order_item(order_item.id).set(relations["invoice_address"])
      RelationWorker.order_item(order_item.id).set(relations["delivery_address"])

      RelationWorker.order_item(order_item.id).set(relations["commodity"])
      RelationWorker.order_item(order_item.id).set(relations["purchase_order"]) if order_item.purchase_order_id.nil?
    end
  end

  def self.import(csv_row, async=true)
    mapper = Mapper::OrderItem.new(csv_row)
    self.perform_async(mapper.attributes, mapper.relations)
  end

end
