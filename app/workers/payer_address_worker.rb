class PayerAddressWorker
  include Sidekiq::Worker
  sidekiq_options :retry => true, :backtrace => true

  def perform(attributes, relations)
    address = PayerAddress.where(:baan_id => attributes["baan_id"]).first
    address ||= PayerAddress.new

    address.attributes = attributes

    address.save

    if relations
      RelationWorker.payer_address(address.id).set(relations["customer"]) if address.customer_id.nil?
    end
  end

  def self.import(csv_row, async=true)
    mapper = Mapper::PayerAddress.new(csv_row)
    self.perform_async(mapper.attributes, mapper.relations)
  end

end
