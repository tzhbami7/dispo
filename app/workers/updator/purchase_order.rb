class Updator::PurchaseOrder
  include Sidekiq::Worker
  sidekiq_options :retry => true, :backtrace => true, :queue => :updator

  def perform(method_name, record_id)
    self.send(method_name, record_id)
  end

  private
  
  def update_aggregations(record_id)
    purchase_order = PurchaseOrder.where(:id => record_id).first

    purchase_order.stocked = purchase_order.order_items.sum(:stocked)
    purchase_order.manufactured = purchase_order.order_items.sum(:manufactured)
    purchase_order.pending = purchase_order.order_items.sum(:pending)

    purchase_order.save
  end

end
