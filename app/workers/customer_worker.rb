class CustomerWorker
  include Sidekiq::Worker
  sidekiq_options :retry => true, :backtrace => true

  def perform(attributes, relations)
    customer = Customer.where(:baan_id => attributes["baan_id"]).first
    customer ||= Customer.new

    customer.attributes = attributes
    customer.save

    if relations
      # RelationWorker.delivery_address(address.id).set(relations["customer"])
    end
  end

  def self.import(csv_row, async=true)
    mapper = Mapper::Customer.new(csv_row)
    self.perform_async(mapper.attributes, mapper.relations)
  end

end
