class RelationWorker
  include Sidekiq::Worker
  sidekiq_options :retry => true, :backtrace => true, :queue => :relation

  def self.owner=(data)
    @owner = data
  end

  def self.owner
    return @owner
  end

  def self.order_item(record_id)
    @owner = {"name" => "OrderItem", "id" => record_id}
    return self
  end

  def self.purchase_order(record_id)
    @owner = {"name" => "PurchaseOrder", "id" => record_id}
    return self
  end

  def self.payer_address(record_id)
    @owner = {"name" => "PayerAddress", "id" => record_id}
    return self
  end

  def self.invoice_address(record_id)
    @owner = {"name" => "InvoiceAddress", "id" => record_id}
    return self
  end

  def self.delivery_address(record_id)
    @owner = {"name" => "DeliveryAddress", "id" => record_id}
    return self
  end

  def self.set(target)
    self.perform_async(self.owner, target)
  end

  def perform(owner, target)
    if !target["id"]
      return nil
    end

    target_key = target["primary_key"] ? target["primary_key"] : "id"

    target_object = target["name"].constantize.where(target_key => target["id"]).first
    owner_object = owner["name"].constantize.where(:id => owner["id"]).first

    if target_object
      owner_object.send("#{target["key"]}=", target_object)
      owner_object.save
    else
      RelationWorker.perform_async(owner, target)
    end
  end

end
