class PurchaseOrderWorker
  include Sidekiq::Worker
  sidekiq_options :retry => true, :backtrace => true

  def perform(attributes, relations)
    purchase_order = PurchaseOrder.where(:baan_id => attributes["baan_id"]).first
    purchase_order ||= PurchaseOrder.new(attributes)

    purchase_order.save

    if relations
      RelationWorker.purchase_order(purchase_order.id).set(relations["payer_address"])
      RelationWorker.purchase_order(purchase_order.id).set(relations["invoice_address"])
      RelationWorker.purchase_order(purchase_order.id).set(relations["delivery_address"])
    end

    Updator::PurchaseOrder.perform_async("update_aggregations", purchase_order.id)
  end

  def self.import(csv_row, async=true)
    mapper = Mapper::PurchaseOrder.new(csv_row)
    self.perform_async(mapper.attributes, mapper.relations)
  end
end
