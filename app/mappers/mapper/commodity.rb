class Mapper::Commodity < Mapper
  def attributes
    @attributes.merge!(:code => self.data[0][1])
    @attributes.merge!(:body => self.data[1][1])

    return @attributes
  end
  
  def relations
    return @relations
  end

end
