class Mapper::PayerAddress < Mapper

  def attributes
    # @attributes.merge!(:eori => self.data[12][1])
    @attributes.merge!(:country => self.data[9][1].strip)
    @attributes.merge!(:baan_id => self.data[55][1])
    @attributes.merge!(:company_name => self.data[56][1].strip)
    @attributes.merge!(:street => [self.data[61][1].strip, self.data[62][1].strip].join(" "))
    @attributes.merge!(:postal_code => self.data[63][1])
    @attributes.merge!(:city => self.data[64][1].strip)

    return @attributes
  end

  def relations
    @relations.merge!("customer" => {"id" => self.data[6][1], "name" => "Customer", "primary_key" => "baan_id", "key" => "customer"})
    return @relations
  end

end
