class Mapper::Customer < Mapper

  def attributes
    @attributes.merge!(:company => self.data[5][1].strip)
    @attributes.merge!(:baan_id => self.data[6][1])

    return @attributes
  end

  def relations
    # @relations.merge!("purchase_order" => {"id" => self.data[2][1], "name" => "PurchaseOrder", "primary_key" => "baan_id", "key" => "purchase_order"})
    return @relations
  end

end
