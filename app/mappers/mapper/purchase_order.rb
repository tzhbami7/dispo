class Mapper::PurchaseOrder < Mapper

  def attributes
    @attributes.merge!(:baan_id => self.data[2][1])
    # @attributes.merge!("delivery_date" => Date.parse(self.data[13][1]))

    return @attributes
  end

  def relations
    @relations.merge!("payer_address" => {"id" => self.data[55][1], "name" => "PayerAddress", "primary_key" => "baan_id", "key" => "payer_address"})
    @relations.merge!("invoice_address" => {"id" => self.data[47][1], "name" => "InvoiceAddress", "primary_key" => "baan_id", "key" => "invoice_address"})
    @relations.merge!("delivery_address" => {"id" => self.data[71][1], "name" => "DeliveryAddress", "primary_key" => "baan_id", "key" => "delivery_address"})

    return @relations
  end

end
