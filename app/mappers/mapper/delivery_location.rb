class Mapper::DeliveryLocation < Mapper

  def attributes
    @attributes.merge!(:name => self.data[35][1].try(:strip))

    return @attributes
  end

  def relations
    return @relations
  end

end
