class Mapper::OrderItem < Mapper
  def attributes
    @attributes.merge!(:baan_id => [self.data[2][1], self.data[4][1]].join("-"))
    @attributes.merge!(:quantity => self.data[18][1].to_i)

    @attributes.merge!(:stocked => self.data[78][1].to_i)
    @attributes.merge!(:manufactured => self.data[79][1].to_i)

    @attributes.merge!(:depot => self.data[23][1].try(:strip))

    @attributes.merge!(:netto_amount => BigDecimal(self.data[40][1]))
    @attributes.merge!(:brutto_amount => BigDecimal(self.data[38][1]))

    @attributes.merge!(:weight => BigDecimal(self.data[15][1]))

    @attributes.merge!(:picked_up => self.data[84][1].to_i)

    return @attributes
  end

  def relations
    @relations.merge!("delivery_group" => {"id" => self.data[21][1].try(:strip), "name" => "DeliveryGroup", "primary_key" => "name", "key" => "delivery_group"})
    @relations.merge!("delivery_location" => {"id" => self.data[35][1].try(:strip), "name" => "DeliveryLocation", "primary_key" => "name", "key" => "delivery_location"})

    @relations.merge!("payer_address" => {"id" => self.data[55][1], "name" => "PayerAddress", "primary_key" => "baan_id", "key" => "payer_address"})
    @relations.merge!("invoice_address" => {"id" => self.data[47][1], "name" => "InvoiceAddress", "primary_key" => "baan_id", "key" => "invoice_address"})
    @relations.merge!("delivery_address" => {"id" => self.data[71][1], "name" => "DeliveryAddress", "primary_key" => "baan_id", "key" => "delivery_address"})

    @relations.merge!("purchase_order" => {"id" => self.data[2][1], "name" => "PurchaseOrder", "primary_key" => "baan_id", "key" => "purchase_order"})
    @relations.merge!("commodity" => {"id" => self.data[0][1], "name" => "Commodity", "primary_key" => "code", "key" => "commodity"})

    return @relations
  end
end
