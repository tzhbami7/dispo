class Mapper::DeliveryGroup < Mapper

  def attributes
    @attributes.merge!(:name => self.data[21][1].try(:strip))

    return @attributes
  end

  def relations
    return @relations
  end

end
