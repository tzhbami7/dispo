class Mapper
  def initialize(data)
    @data = data
    @attributes = Hash.new
    @relations = Hash.new
  end

  def data
    return @data
  end

end
