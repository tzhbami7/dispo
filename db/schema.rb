# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20140610161039) do

  create_table "articles", :force => true do |t|
    t.string   "name"
    t.string   "article_number"
    t.string   "baan_id"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "commodities", :force => true do |t|
    t.integer  "code"
    t.string   "body"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "customers", :force => true do |t|
    t.string   "company"
    t.boolean  "active",     :default => true
    t.string   "baan_id"
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
  end

  create_table "deliveries", :force => true do |t|
    t.boolean  "active"
    t.date     "delivery_date"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "delivery_addresses", :force => true do |t|
    t.string   "street"
    t.integer  "postal_code"
    t.string   "city"
    t.string   "country"
    t.string   "company_name"
    t.string   "eori"
    t.integer  "customer_id"
    t.string   "baan_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "delivery_groups", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "delivery_locations", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "importers", :force => true do |t|
    t.string   "baan_file"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "invoice_addresses", :force => true do |t|
    t.string   "street"
    t.integer  "postal_code"
    t.string   "city"
    t.string   "country"
    t.string   "company_name"
    t.string   "eori"
    t.integer  "customer_id"
    t.string   "baan_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "line_items", :force => true do |t|
    t.integer  "pallet_id"
    t.integer  "variant_id"
    t.integer  "order_item_id"
    t.integer  "quantity"
    t.decimal  "brutto_weight", :precision => 12, :scale => 2, :default => 0.0, :null => false
    t.decimal  "brutto_amount", :precision => 12, :scale => 2, :default => 0.0, :null => false
    t.decimal  "netto_weight",  :precision => 12, :scale => 2, :default => 0.0, :null => false
    t.decimal  "netto_amount",  :precision => 12, :scale => 2, :default => 0.0, :null => false
    t.datetime "created_at",                                                    :null => false
    t.datetime "updated_at",                                                    :null => false
  end

  create_table "order_items", :force => true do |t|
    t.string   "baan_id"
    t.integer  "purchase_order_id"
    t.integer  "article_id"
    t.integer  "commodity_id"
    t.integer  "delivery_address_id"
    t.integer  "invoice_address_id"
    t.integer  "payer_address_id"
    t.integer  "delivery_group_id"
    t.integer  "delivery_location_id"
    t.integer  "stocked",                                             :default => 0
    t.integer  "manufactured",                                        :default => 0
    t.integer  "pending",                                             :default => 0
    t.string   "depot"
    t.integer  "quantity"
    t.integer  "available_quantity"
    t.decimal  "weight",               :precision => 12, :scale => 2, :default => 0.0,   :null => false
    t.decimal  "netto_amount",         :precision => 12, :scale => 2, :default => 0.0,   :null => false
    t.decimal  "brutto_amount",        :precision => 12, :scale => 2, :default => 0.0,   :null => false
    t.boolean  "picked_up",                                           :default => false
    t.boolean  "deleted",                                             :default => false
    t.datetime "created_at",                                                             :null => false
    t.datetime "updated_at",                                                             :null => false
  end

  create_table "pallets", :force => true do |t|
    t.integer  "purchase_order_id"
    t.integer  "delivery_id"
    t.integer  "pallet_type_id"
    t.integer  "shipping_address_id"
    t.decimal  "brutto_weight",       :precision => 12, :scale => 2, :default => 0.0,   :null => false
    t.decimal  "brutto_amount",       :precision => 12, :scale => 2, :default => 0.0,   :null => false
    t.decimal  "netto_weight",        :precision => 12, :scale => 2, :default => 0.0,   :null => false
    t.decimal  "netto_amount",        :precision => 12, :scale => 2, :default => 0.0,   :null => false
    t.float    "discount_rate",                                      :default => 0.0,   :null => false
    t.boolean  "delivered",                                          :default => false
    t.boolean  "mixed",                                              :default => false
    t.integer  "line_items_count",                                   :default => 0
    t.datetime "created_at",                                                            :null => false
    t.datetime "updated_at",                                                            :null => false
  end

  create_table "payer_addresses", :force => true do |t|
    t.string   "street"
    t.integer  "postal_code"
    t.string   "city"
    t.string   "country"
    t.string   "company_name"
    t.string   "eori"
    t.integer  "customer_id"
    t.string   "baan_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "purchase_orders", :force => true do |t|
    t.string   "baan_id"
    t.integer  "delivery_address_id"
    t.integer  "invoice_address_id"
    t.integer  "payer_address_id"
    t.integer  "delivery_group_id"
    t.integer  "delivery_location_id"
    t.integer  "stocked",                                                :default => 0
    t.integer  "manufactured",                                           :default => 0
    t.integer  "pending",                                                :default => 0
    t.boolean  "stocking_completed",                                     :default => false
    t.boolean  "manufacturing_completed",                                :default => false
    t.decimal  "weight",                  :precision => 12, :scale => 2, :default => 0.0,   :null => false
    t.decimal  "netto_amount",            :precision => 12, :scale => 2, :default => 0.0,   :null => false
    t.decimal  "brutto_amount",           :precision => 12, :scale => 2, :default => 0.0,   :null => false
    t.boolean  "picked_up",                                              :default => false
    t.boolean  "deleted",                                                :default => false
    t.datetime "created_at",                                                                :null => false
    t.datetime "updated_at",                                                                :null => false
  end

  create_table "variants", :force => true do |t|
    t.integer  "order_item_id"
    t.integer  "line_item_id"
    t.decimal  "brutto_weight", :precision => 12, :scale => 2, :default => 0.0, :null => false
    t.decimal  "brutto_amount", :precision => 12, :scale => 2, :default => 0.0, :null => false
    t.decimal  "netto_weight",  :precision => 12, :scale => 2, :default => 0.0, :null => false
    t.decimal  "netto_amount",  :precision => 12, :scale => 2, :default => 0.0, :null => false
    t.datetime "created_at",                                                    :null => false
    t.datetime "updated_at",                                                    :null => false
  end

end
