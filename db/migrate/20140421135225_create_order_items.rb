class CreateOrderItems < ActiveRecord::Migration
  def change
    create_table :order_items do |t|
      t.string :baan_id
      t.integer :purchase_order_id
      t.integer :article_id
      t.integer :commodity_id

      t.integer :delivery_address_id
      t.integer :invoice_address_id
      t.integer :payer_address_id

      t.integer :delivery_group_id
      t.integer :delivery_location_id
      
      t.integer :stocked, :default => 0
      t.integer :manufactured, :default => 0
      t.integer :pending, :default => 0

      t.string :depot

      t.integer :quantity
      t.integer :available_quantity

      t.decimal :weight, :precision => 12, :scale => 2, :default => 0.0, :null => false

      t.decimal :netto_amount, :precision => 12, :scale => 2, :default => 0.0, :null => false
      t.decimal :brutto_amount, :precision => 12, :scale => 2, :default => 0.0, :null => false

      t.boolean :picked_up, :default => false
      t.boolean :deleted, :default => false

      t.timestamps
    end
  end
end
