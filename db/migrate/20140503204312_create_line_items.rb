class CreateLineItems < ActiveRecord::Migration
  def change
    create_table :line_items do |t|

      t.integer :pallet_id
      t.integer :variant_id
      t.integer :order_item_id

      t.integer :quantity
      t.decimal :brutto_weight, :precision => 12, :scale => 2, :default => 0.0, :null => false
      t.decimal :brutto_amount, :precision => 12, :scale => 2, :default => 0.0, :null => false
      t.decimal :netto_weight, :precision => 12, :scale => 2, :default => 0.0, :null => false
      t.decimal :netto_amount, :precision => 12, :scale => 2, :default => 0.0, :null => false

      t.timestamps
    end
  end
end
