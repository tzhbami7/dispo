class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.string :name
      t.string :article_number
      t.string :baan_id

      t.timestamps
    end
  end
end
