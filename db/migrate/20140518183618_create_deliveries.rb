class CreateDeliveries < ActiveRecord::Migration
  def change
    create_table :deliveries do |t|
      t.boolean :active
      t.date :delivery_date

      t.timestamps
    end
  end
end
