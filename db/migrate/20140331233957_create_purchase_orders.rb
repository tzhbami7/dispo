class CreatePurchaseOrders < ActiveRecord::Migration
  def change
    create_table :purchase_orders do |t|
      t.string :baan_id

      t.integer :delivery_address_id
      t.integer :invoice_address_id
      t.integer :payer_address_id

      t.integer :delivery_group_id
      t.integer :delivery_location_id

      t.integer :stocked, :default => 0
      t.integer :manufactured, :default => 0
      t.integer :pending, :default => 0

      t.boolean :stocking_completed, :default => false
      t.boolean :manufacturing_completed, :default => false

      t.decimal :weight, :precision => 12, :scale => 2, :default => 0.0, :null => false

      t.decimal :netto_amount, :precision => 12, :scale => 2, :default => 0.0, :null => false
      t.decimal :brutto_amount, :precision => 12, :scale => 2, :default => 0.0, :null => false

      t.boolean :picked_up, :default => false
      t.boolean :deleted, :default => false

      t.timestamps
    end
  end
end
