class CreateInvoiceAddresses < ActiveRecord::Migration
  def change
    create_table :invoice_addresses do |t|
      t.string  "street"
      t.integer "postal_code"
      t.string  "city"
      t.string  "country"
      t.string  "company_name"
      t.string  "eori"

      t.integer "customer_id"
      t.string  "baan_id"

      t.timestamps
    end
  end
end
