class CreatePallets < ActiveRecord::Migration
  def change
    create_table :pallets do |t|

      t.integer :purchase_order_id
      t.integer :delivery_id
      t.integer :pallet_type_id
      t.integer :shipping_address_id
      
      t.decimal :brutto_weight, :precision => 12, :scale => 2, :default => 0.0, :null => false
      t.decimal :brutto_amount, :precision => 12, :scale => 2, :default => 0.0, :null => false
      t.decimal :netto_weight, :precision => 12, :scale => 2, :default => 0.0, :null => false
      t.decimal :netto_amount, :precision => 12, :scale => 2, :default => 0.0, :null => false
      t.float :discount_rate, :default => 0.0, :null => false

      t.boolean :delivered, :default => false
      t.boolean :mixed, :default => false

      t.integer :line_items_count, :default => 0

      t.timestamps
    end
  end
end
