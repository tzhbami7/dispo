class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.string   "company"
      t.boolean  "active", :default => true
      t.string   "baan_id"

      t.timestamps
    end
  end
end
