class CreateDeliveryGroups < ActiveRecord::Migration
  def change
    create_table :delivery_groups do |t|
      t.string :name

      t.timestamps
    end
  end
end
