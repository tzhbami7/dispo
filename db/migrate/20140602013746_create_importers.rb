class CreateImporters < ActiveRecord::Migration
  def change
    create_table :importers do |t|
      t.string :baan_file

      t.timestamps
    end
  end
end
