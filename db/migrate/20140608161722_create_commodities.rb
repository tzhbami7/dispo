class CreateCommodities < ActiveRecord::Migration
  def change
    create_table :commodities do |t|
      t.integer :code
      t.string :body
      
      t.timestamps
    end
  end
end
