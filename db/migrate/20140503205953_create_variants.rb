class CreateVariants < ActiveRecord::Migration
  def change
    create_table :variants do |t|

      t.integer :order_item_id
      t.integer :line_item_id
      
      t.decimal :brutto_weight, :precision => 12, :scale => 2, :default => 0.0, :null => false
      t.decimal :brutto_amount, :precision => 12, :scale => 2, :default => 0.0, :null => false
      t.decimal :netto_weight, :precision => 12, :scale => 2, :default => 0.0, :null => false
      t.decimal :netto_amount, :precision => 12, :scale => 2, :default => 0.0, :null => false

      t.timestamps
    end
  end
end
