# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Article.create(:name => "77 Mineralgusswaschtisch900 mm", :article_number => "SP00109547709300 0001")
Article.create(:name => "Unterbau neo.2 900 mm", :article_number => "3000139957721300 0001")
Article.create(:name => "Spiegelschrank MyStyle 900 mm", :article_number => "3000139955941500 0001")
Article.create(:name => "FRONT re zu UB Neo 1200mm", :article_number => "3000144174821600 0001")
Article.create(:name => "Unterbau Noto 1550mm", :article_number => "SP00111976120100 0001")
Article.create(:name => "Unterschrank MyStyle 590 mm", :article_number => "3000144245925400 0001")
Article.create(:name => "Unterbau MyStar 1220mm", :article_number => "3000144418424101 0001")
Article.create(:name => "Spiegelschrank MyStyle 900 mm", :article_number => "3000145255940500 0001")

initial_baan_id = "2271122440"

(1..10).each do |i|
  purchase_order = PurchaseOrder.new(:baan_id => initial_baan_id + "#{i}")
  (1..10).each do |ii|
    article = Article.order("RANDOM()").first
    order_item = purchase_order.order_items.build(:baan_id => "#{purchase_order.baan_id}-#{ii}", :quantity => rand(1..10))
    order_item.article = article
  end
  purchase_order.save
end